\input{./res/header.tex}														

\newcommand{\abstractcontent}{
	Linear regression is used to find a linear relationship between independent variables and outcomes of an experiment dependent on those variables. In this tutorial
	we are going to find a model which describes this linear relationship between variables and outcomes with respect to some parameters. Then, we are going to
	define a reasonable criterion for how accurate our model is given specific experiment data. Based on this criterion, we are going to derive a solution for
	finding the optimal model parameters.
	
	We are then going to generalize this idea and talk about how linear regression can be used for polynomial approximation and compare the results with the ones
	obtained using polynomial interpolation. Then we are going to use linear regression for solving a classification problem, in particular for recognizing
	handwritten digits.
}

\begin{document}

	\input{./res/titlepage.tex}										
	
	\section{Motivation}
		Given a set of points $(x_1, y_1), \dots, (x_n, y_n)$ we are looking for a linear function which, given $x_i$, best approximates $y_i$.
		We are assuming each $x_i$ is the data with which an experiment was conducted and $y_i$ is the measured result of this experiment, with some small
		measurement error.
		
		In other words, we can define our experiment as a function $f(x)$ such that:
		
		\begin{align}
			f(x_i) &= \hat{y_i} \\
			\hat{f(x_i)} &= f(x_i) + \mathcal{N}(\mu,\,\sigma^{2}) \\
				&= \hat{y_i} + \mathcal{N}(\mu,\,\sigma^{2}) \\
				&= y_i
		\end{align}
		
		Here $\mathcal{N}(\mu,\,\sigma^{2})$ is a small measurement error, denoted as a sampled point from a normal distribution with mean $\mu$ and variance $\sigma^2$.
		Equation (1) models the outcome of an experiment with variables $x_i$. Equation (2) models the outcomes we measure when conducting 
		an experiment with variables $x_i$. Our task is to find a good approximation of $f(x)$ using observations of $\hat{f(x)}$.
		
		In a 2-dimensional case where $x_i$ is just a point on the x-axis, we are looking for $f(x)$ such that:
		
		\begin{align*}
			f(x) = ax + b
		\end{align*}
		
		Figure \ref{fig:2dreg} shows an example of such a scenario and the optimal $f(x)$ line with slope $a$ and bias $b$.
	
		\begin{figure}[h!]
			\includegraphics[width=\textwidth]{./plots/2d_regression.png}
			\caption{Linear regression in 2D}
			\label{fig:2dreg}
		\end{figure}
	
		\clearpage
	
	\section{Model accuracy criterion (Mean squared error)}
		Using the 2-dimensional model defined above, we are looking for $f(x)$, such that
		
		\begin{align*}
			& \forall i \in \{1, \dots, n\}: f(x_i) = a x_i + b = y_i \\
			\iff & \forall i \in \{1, \dots, n\}: f(x_i) - y_i = (a x_i + b) - y_i = 0 
		\end{align*}
		
		However, as one can see from the example above, such a function doesn't exist, as there is no line which goes through all points. We need to find the
		best possible line, therefore we can define our error for each data point as being $f(x_i) - y_i$ and try to minimize this error. As we want
		$f(x_i) = y_i$, we actually want to minimize the absolute value, so as to minimize the deviation between prediction and observation.
		Therefore, we use the mean squared error ($MSE$).
		
		\begin{align}
			MSE((x_1, y_1), \dots, (x_n, y_n)) &= \frac{1}{n}\sum_{i = 0}^{n} (f(x_i) - y_i)^2 \\
				&= \frac{1}{n}\sum_{i = 0}^{n} (a x_i + b - y_i)^2
		\end{align}
		
	
	\section{m-dimensional scenario}
		
		If $x_i$ is an m-dimensional variable, our linear model becomes
		
		\begin{align*}
			f(x_i) = \omega_1 x_{i,1} + \omega_2 x_{i, 2} + \dots + \omega_m x{i, m} + b
		\end{align*}
		
		We are now going to redefine our problem in matrix notation, as the derivation of the optimal parameters $\omega_1,  \dots \omega_m$ will be much easier in this notation.
		First, we observe we can represent $f(x_i)$ as just a vector-vector multiplication, as
		
		\begin{align*}
			f(x_i) &= \omega_1 x_{i,1} + \omega_2 x_{i, 2} + \dots + \omega_m x{i, m} + b \\
				   &= [x_{i, 1}, \dots, x_{i,m}, 1] \begin{bmatrix}
				   		\omega_{1} \\
				   		\vdots \\
				   		\omega_{m} \\
				   		b
				   \end{bmatrix}
		\end{align*}
		
		We define our experiment variables $x_1, \dots, x_n$ as a matrix $\hat{X}$ such that
		
		\begin{align*}
			\hat{X} &= \begin{bmatrix}
			x_{1} \\
			x_{2} \\
			\vdots \\
			x_{n}
			\end{bmatrix}  \\
				&= \begin{bmatrix}
				x_{1,1} & x_{1,2} & \dots & x_{1,m} \\
				x_{2,1} & x_{2,2} & \dots & x_{2,m} \\
				\vdots & & & \vdots \\
				x_{n,1} & x_{n,2} & \dots & x_{n,m} \\
				\end{bmatrix} \in Mat_{n \times m}(\mathbb{R})
		\end{align*}
		
		To use the observation above and represent the model with just a matrix-vector multiplication, we then define
		
		\begin{align*}
			X &= \begin{bmatrix}
				& & 1 \\
				& \hat{X} & \vdots \\
				& & 1
			\end{bmatrix}
		\end{align*}
		
		We define our model parameters $\omega_1,  \dots \omega_m, b$ as a vector
		
		\begin{align*}
			\omega &= \begin{bmatrix}
				\omega_{1} \\
				\omega_{2} \\
				\vdots \\
				\omega_{m} \\
				b
			\end{bmatrix}
		\end{align*}		
		
		and our experiment observations as a vector
		
		\begin{align*}
			y &= \begin{bmatrix}
				y_{1} \\
				y_{2} \\
				\vdots \\
				y_{n}
			\end{bmatrix}
		\end{align*}
		
		Note that the previous sum of squared errors over single data points and expectations 
		$\sum_{i = 0}^{n} (a x_i + b - y_i)^2$ is in matrix notation expressed as $(X \omega - y)^{T} (X \omega - y)$.
		Let's observe the mean squared error
		
		\begin{align}
			MSE(X, y) &= \frac{1}{n}(X \omega - y)^{T} (X \omega - y) \\
				&= \frac{1}{n} ((X \omega)^T - y^T)(X \omega - y) \\ 
				&= \frac{1}{n} ( (\omega^T X^T - y^T)(X \omega - y)  )\\
				&= \frac{1}{n} ( \omega^T X^T X \omega - \omega^T X^T y - y^T X \omega + y^Ty )\\
				&= \frac{1}{n} ( \omega^T X^T X \omega - 2 \omega^T X^T y + y^Ty )
		\end{align}
		
		Note that $(7) = (8)$ as $\omega^T X^T y = (y^T X \omega)^T$ and both are scalars. Further, for all scalars $\forall x \in \mathbb{R}: x^T = x$
		
	
	\section{Deriving least squares solution to linear regression}
	
		\subsection{Minimizing the mean squared error}
		
			We know the mean squared error is a quadratic function with non-negative values. Therefore, it has only one extreme, which is it's minimum.
			Therefore, the optimal parameters $\omega$ are those for which $MSE$ is minimal. We can find those by finding the $\omega$ for which $\frac{\partial MSE(X, y)}{\partial \omega} = 0$.
			
			\begin{align*}
				& \frac{\partial MSE(X, y)}{\partial \omega} &= & 0 \\
					\iff & \frac{\partial n MSE(X, y)}{\partial \omega} &= & 0  
														\tag{derivative multiplied by a constant factor has same roots} \\
					\iff & \frac{\partial \omega^T X^T X \omega - 2 \omega^T X^T y + y^Ty}{\partial} &= & 0 
														\tag{$\frac{1}{n} n = 1$} \\
					\iff & \frac{\partial \omega^T X^T X \omega - 2 \omega^T X^T y}{\partial} &= & 0 
														\tag{$\frac{\partial y^Ty}{\partial \omega} = 0$} \\
					\iff & \frac{\partial \omega^T X^T X \omega}{\partial \omega} - \frac{2 \omega^T X^T y}{\partial} &= & 0 \\
					\iff & \frac{\partial \omega^T X^T X \omega}{\partial \omega} &= & \frac{2 \omega^T X^T y}{\partial} \\
			\end{align*}
		
		\subsection{Finding matrix derivatives}
		
			Let's start with $\frac{\partial 2 \omega^T X^T y}{\partial \omega}$.
			
			We simplify the situation by observing that $X^T y$ is just a vector. Therefore, for a vectors $v, x \in \mathbb{R}^k$ we find
			
			\begin{align*}
				& \frac{\partial x^T v}{\partial x} \\
				&= \frac{\partial x_1 v_1 + x_2 v_2 + \dots + x_k v_k}{\partial x} \\
				&= \begin{bmatrix}
					\frac{\partial x_1 v_1 + x_2 v_2 + \dots + x_k v_k}{\partial x_1} \\
					\vdots \\
					\frac{\partial x_1 v_1 + x_2 v_2 + \dots + x_k v_k}{\partial x_k} \\
				\end{bmatrix} \\
				& = \begin{bmatrix}
					v_1 \\
					\vdots \\
					v_k
				\end{bmatrix} \\
				& = v
			\end{align*}
			
			$\implies \frac{\partial 2 \omega^T X^T y}{\partial \omega} = 2 X^T y $
		
			\rule{\linewidth}{0.8pt}
			\vspace{1cm}
			
			
			Finding the partial derivative $\frac{\partial \omega^T X^T X \omega}{\partial \omega}$ is a little more complicated though. We observe $X^TX$ is a symmetric matrix,
			as clearly $(X^TX)^T = X^TX$. So we can simplify the situation by looking for the derivative $\frac{\partial x^T A x}{\partial x}$ for $x \in \mathbb{R}^k, A \in Mat_{k \times k}(\mathbb{R})$.
			
			A symmetric square matrix can be uniquely represented by a quadratic form as learned in Linear Algebra II \cite{laii}, so we could omit the calculation of
			$x^T A x$. For the sake of completeness, we show this result anyways. 
			
			Note that as A is symmetric, that means $A = 
			\begin{bmatrix}
				a_{1,1} & a_{1,2} & \dots & a_{1, k} \\
				a_{2,1} & a_{2,2} & \dots & a_{2, k} \\
				\vdots & \vdots & \ddots & \vdots \\
				a_{k, 1} & a_{k, 2} & \dots & a_{k, k}
			\end{bmatrix} = 
			\begin{bmatrix}
			a_{1,1} & a_{1,2} & \dots & a_{1, k} \\
			a_{1,2} & a_{2,2} & \dots & a_{2, k} \\
			\vdots & \vdots & \ddots & \vdots \\
			a_{1, k} & a_{2, k} & \dots & a_{k, k}
			\end{bmatrix}
			$
			
			\begin{align*}
				\frac{\partial x^T A X}{\partial x} &= \frac{\partial \sum_{i = 0}^{k} \sum_{j = 0}^{k} x_i x_j a_{i, j} } {\partial x} \\
					&=  \frac{\partial \sum_{i = 0}^{k} x_i^2 a_{i, i} + \sum_{j = 0}^k\sum_{l \neq j} x_j x_l a_{j, l} }{\partial x} \\
					&=  \frac{\partial \sum_{i = 0}^{k} x_i^2 a_{i, i} + \sum_{j = 0}^k\sum_{l > j} 2 x_j x_l a_{j, l} }{\partial x} 
																					\tag{because $a_{i, j} = a_{j, i}$ as A is symmetric} \\
					&= \begin{bmatrix}
						\frac{\partial \sum_{i = 0}^{k} x_i^2 a_{i, i} + \sum_{j = 0}^k\sum_{l > j} 2 x_j x_l a_{j, l} }{\partial x_1} \\
						\vdots \\
						\frac{\partial \sum_{i = 0}^{k} x_i^2 a_{i, i} + \sum_{j = 0}^k\sum_{l > j} 2 x_j x_l a_{j, l} }{\partial x_k}
					\end{bmatrix} \\
					&= \begin{bmatrix}
						2 a_{1,1} x_1 + \dots + 2 a_{1, k} x_k \\
						\vdots \\
						2 a_{1, k} x_1 + \dots + 2 a_{k, k} x_k
					\end{bmatrix} \\
					&= \begin{bmatrix}
					2 a_{1,1} x_1 + \dots + 2 a_{1, k} x_k \\
					\vdots \\
					2 a_{k,1} x_1 + \dots + 2 a_{k, k} x_k
					\end{bmatrix} \\
					&= 2Ax
			\end{align*}
			
			$\implies \frac{\partial \omega^T X^T X \omega}{\partial \omega} = 2 X^T X \omega$
			
		\clearpage
			
		\subsection{Combining results}
			Combining the results of the previous two sections, we  arrive at the conclusion
			
			\begin{align*}
				& \frac{\partial MSE(X, y)}{\partial \omega} &= & 0 \\
					\iff & \frac{\partial \omega^T X^T X \omega}{\partial \omega} &= & \frac{2 \omega^T X^T y}{\partial} \\
					\iff & 2 X^T X \omega &=& 2 X^T y \\
					\iff & X^T X \omega &=& X^T y \\
					\iff & \omega &=& (X^T X)^{-1} X^T y
			\end{align*}
			
			So the optimal least squares solution to linear regression is $\omega = (X^T X)^{-1} X^T y$.
			However, we are not done yet, as the matrix $X^TX$ is possibly singular and therefore not invertable (one could actually show in which cases it is singular,
			we are omitting this step here though as it is out of the scope of the current tutorial).
	
		\subsection{Inverting possibly singular matrix}
		A singular matrix is a square non-invertible matrix. We know a square matrix $A$ is singular if and only if $det(A) = 0$, which could be the case for $X^TX$ and is
		the problem to solve in this section.
		
		
		In order to invert a singular matrix, often the Moore-Penrose pseudoinverse is used \cite{moorepen}, however this inverse includes
		complex matrix decompositions such as singular value decomposition (SVD), which are beyond the scope of this tutorial. We are going to 
		focus on one special matrix here, $A + I\epsilon$ and explain why it's always invertible if A is a positive semidefinite square matrix.
		
		As we know from "Lineare Algebra für Informatiker" \cite{lai}, the characteristic polynomial of  $A \in Mat_{n \times n}(\mathbb{R})$ is
		$\chi_A(\lambda) = det(X - I \lambda)$. Further we know
		
		\begin{align*}
			& \chi_A(\lambda) = 0 \\
			\iff & det(A - I \lambda) = 0 \\
			\iff & Ker(A - I \lambda) \neq \emptyset \\
			\iff & Eig(A, \lambda) \neq \emptyset \\
			\iff & \lambda \text{ is an eigenvalue of } A
		\end{align*}
		
		But further, as learned in Linear Algebra II \cite{laii}, $\forall x \in \mathbb{R}^k: xAx \geq 0 \iff (\forall \lambda_i: Eig(A, \lambda_i) \neq \emptyset \iff \lambda_i > 0)$.
		Or informally, a positive semidefinite symmetric matrix A has only positive eigenvalues. \\
		
		$\implies \forall \epsilon > 0: det(X^TX + I\epsilon) = det(X^TX - I(-\epsilon)) \neq 0$ \\
		
		Otherwise, this would mean a negative $\epsilon$ is an eigenvalue of $X^TX$, which would be a contradiction to the previous statements.
		Therefore, we could use $(X^TX + I\epsilon)^{-1}$ instead of $(X^TX)^{-1}$ for some very small $\epsilon$.
	
	\section{Polynomial approximation}
	
		Linear regression is only linear in the parameter space $\omega$. However, we could apply a non-linear transformation to the data set before applying linear regression.
		
		In a 2-dimensional scenario we could transform the matrix $X$ from above
		
		\begin{align*}
			X = \begin{bmatrix*}
				x_{1,1} & 1 \\
				\vdots & \vdots \\
				x_{n,1} & 1
			\end{bmatrix*}
		\end{align*}
		
		into k-dimensional data
	
		\begin{align*}
			X_k = & \begin{bmatrix*}
				x_{1,1} & x_{1,2} & \dots & x_{1,k} & 1 \\
				\vdots & & & \vdots \\
				x_{n,1} & x_{n,2} & \dots & x_{n,k} & 1 \\
			\end{bmatrix*} \\
			=& \begin{bmatrix*}
				x_{1,1} & x_{1,1}^2 & \dots & x_{1,1}^k & 1 \\
				\vdots & & & \vdots \\
				x_{n,1} & x_{n,1}^2 & \dots & x_{n,1}^k & 1 \\
			\end{bmatrix*}
		\end{align*}
		
		This way, we can approximate a polynomial of degree $k$. Often it makes sense to increment the degree $k$ until one is happy with the mean squared error.
		Otherwise, one might have a very precise model when tested on the data the model is supposed to fit, but a very inaccurate one when tested on unseen data. This
		phenomenon is known as "overfitting" the data. Figure \ref{fig:polapint} compares results of polynomial approximation and polynomial interpolation.
		
		\begin{figure}[h!]
			\includegraphics[width=\textwidth]{./plots/2d_pol_ap_int.png}
			\caption{Polynomial approximation VS interpolation}
			\label{fig:polapint}
		\end{figure}
	
		\clearpage
		
		
	\section*{Classification}
	
		One could also use linear regression to solve a classification problem. For this example, we are going to distinguish between hand-written digits
		using the famous MNIST \cite{mnist} data set. \\
		
		The data set consist of $28 \times 28$ pixel images. Our algorithm needs to predict the digit $y_i \in {0, \dots, 9}$ given an image $\widetilde{x_i} = \begin{bmatrix*}
			p_{i, 1, 1} & \dots & p_{i, 1, 28} \\
			\vdots & \ddots & \vdots \\
			p_{i, 28, 1} & \dots & p_{i, 28, 28}
		\end{bmatrix*} \in Mat_{28 \times 28}([0, 1]^2) $, for $ [0, 1]^2 \subset \mathbb{R}^2$
		
		First, we are transforming each image into a $28 \times 28$ - dimensional row vector
		
		\begin{align*}
				\hat{x_i} = [p_{i, 1, 1}, \dots, p_{i, 1, 28}, p_{i,2,1}, \dots, p_{i,2,28}, \dots, p_{i, 28, 28}]
		\end{align*}
		
		Then, similarly to previous sections, we define
		
		\begin{align*}
			X = 
				\begin{bmatrix}
					\hat{x_1} & 1 \\
					\vdots & \vdots \\
					\hat{x_n} \vdots & 1 \\
				\end{bmatrix}
			  =
			  	\begin{bmatrix}
				  	x_1 \\
				  	\vdots \\
				  	x_n \\
			  	\end{bmatrix}
		\end{align*}
		
			\subsection{Distinguishing between two digits}
			
			In order to distinguish between digits i and j, we only take the corresponding $X_i, X_j$ with
			
			\begin{align*}
				X_k := \{ x_i \in X | y_i = k \}
			\end{align*}
			
			Then, we train a binary classifier, which distinguishes between i and j. \\
			
			We do this by training a linear regression to predict, for $x_k \in X_i \cup X_j$
			
			\begin{align*}
				f(x_k) = \begin{cases}
					1 				& y_k = i \\
					-1              & \text{otherwise}
				\end{cases}
			\end{align*}
			
			Or in other words, we have a dataset of $x_k = [x_{k, 1}, \dots, x_{k, 28 \times 28}]$ and $y_k \in {i, j}$.
			Then we train a linear regression as discussed in previous sections using data $(x_1, y_1), \dots, (x_{|X_i \cup X_j|}, y_{|X_i \cup X_j|})$
			
			Then, in order to predict $\hat{y_l}$, which is whether a given image vector $x_l$ is a digit i or digit j, we use
			
			\begin{align*}
				 \hat{y_l} = \begin{cases}
					i 				& x_l^T \omega > 0 \\
					j              & \text{otherwise}
				\end{cases}
			\end{align*}
		
	\clearpage

	\bibliographystyle{unsrt}
	\bibliography{bib}


\end{document}
