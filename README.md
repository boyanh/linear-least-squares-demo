### Running demos

The file `environment.yml` defines all required dependencies and can be used to
automatically create an anaconda virtual environment. However, you could
also make sure all dependencies are installed in a python environment of your
choice and completely skip this guide.

1. Install anaconda
    * follow the instructions in 
        [the official anaconda documentation](https://conda.io/docs/user-guide/install/index.html)
1. Create a virtual environment using the configuration file from terminal
    (should be similar for non *nix systems, check the documentation though)
    ```console
    $ conda create -f environment.yml
    ```
1. Install an ipython kernel using the new python environment
    ```console
    $ source activate lls
    $ python -m ipykernel install --user --name lls --display-name "Python (lls)"
    ```
1. Start jupyter notebook server
    ```console
    $ jupyter notebook
    ```
1. A browser tab should now be opened. Select the demo notebook (lls_demo.ipynb)
    and make sure the "Python (lls)" kernel is being used